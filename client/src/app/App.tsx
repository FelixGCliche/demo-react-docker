import './App.css';
import { ServerLogger } from "../modules/server_logger";

function App() {
  return (
      <div>
        This is the main application
        <div><ServerLogger/></div>
      </div>

  );
}

export default App;
