import axios from 'axios';


function callServer(): string {
    let retval: string = "";
    axios.get(`http://localhost:${process.env.REACT_APP_SERVER_PORT}/`)
        .then((response) => {
            retval = response.data["message"]
        });
    console.log(retval)
    return retval;
}


export function ServerLogger() {
    return (
        <div>{"This is the server logger module: " + callServer()}</div>
    );
}
