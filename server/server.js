const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());

app.get("/", function (req, res) {
    res.send("Hello Node.js")
});

app.listen(process.env.REACT_APP_SERVER_PORT, () => {
    console.log(`App server now listening on port ${process.env.REACT_APP_SERVER_PORT}`);
});